from django.urls import *
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from testing.forms import LoginForm

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('accounts/login/', auth_views.LoginView.as_view(form_class=LoginForm), name='login'),
    path('accounts/logout/', auth_views.LogoutView.as_view(template_name='index.html'), name='logout'),
    re_path('delete/(?P<pk>\d+)', views.delete, name='delete')
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)