from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import FileForm
from .models import Doc
# Create your views here.

def index(request):
    if request.method == 'POST':
        form = FileForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/')
    else:
        form = FileForm()
        doc = Doc.objects.all()
        return render(request, 'index.html', {'form' : form, 'doc' : doc})

def delete(request, pk):
    Doc.objects.get(pk=pk).delete()
    return HttpResponseRedirect('/')