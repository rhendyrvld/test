from django.test import TestCase

# Create your tests here.
from django.test import Client
from django.urls import resolve
from .views import *

class Testing(TestCase):
    def test_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_page_use_index_func(self):
        function = resolve('/')
        self.assertEqual(function.func, index)