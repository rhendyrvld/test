from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from .models import Doc

class LoginForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class' : 'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class' : 'form-control'}))

class FileForm(forms.ModelForm):
    class Meta:
        model = Doc

        fields = ['document_name', 'doc']

        widgets = {
            'document_name' : forms.TextInput(attrs={'class' : 'form-control'}),
            'doc' : forms.ClearableFileInput(attrs={'class' : 'form-control'}),
        }