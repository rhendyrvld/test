from django.db import models

# Create your models here.
class Doc(models.Model):
    document_name = models.CharField(max_length=30)
    doc = models.FileField(upload_to='uploads/')

    def __str__(self):
        return self.document_name